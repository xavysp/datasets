"""
This python code attempt to change the name of a list of files in a directory

"""
import numpy as np
import os
import sys
import cv2 as cv
import scipy.io as sio
import xml.etree.ElementTree as ET

from PIL import Image, ImageDraw
from collections import defaultdict
# from utili import utls


def cv_imshow(label="image",img=None, read_image=False):

	if not read_image:
		cv.imshow(label,img)
		cv.waitKey(0)
		cv.destroyAllWindows()
	else:
		cv.imshow(label, cv.imread(img))
		cv.waitKey(0)
		cv.destroyAllWindows()

def draw_seg_wibd(json_data, image_size, dataset_name=None):

    labels= json_data['Label']
    labels = labels['objects']
    edge_img = np.zeros((image_size[0],image_size[1]))
    edge_imgs =np.empty([image_size[0],image_size[1],1])


    if len(labels)>0:
        n_labels = len(labels)
    else:
        print("there is/are inconsistency in labels and lbl_names size")
        print("Image name:", json_data['External ID'])
        return None

    i_lbls = 0

    for single_label in labels:
        indexes = []
        label_name = single_label['value']
        if label_name=='polygon':
            for x_y_points in single_label['polygon']:
                x = x_y_points['y']  # x = tmp_pts['geometry'][i]['y'] #
                y = x_y_points['x'] # y = tmp_pts['geometry'][i]['x']# y = tmp_pts[i]['x']
                indexes.append(y)
                indexes.append(x)
            edge_img = draw_edges(image_size, indexes, True)
        elif label_name=='polyline':
            for x_y_points in single_label['line']:
                x = x_y_points['y']  # x = tmp_pts['geometry'][i]['y'] #
                y = x_y_points['x']  # y = tmp_pts['geometry'][i]['x']# y = tmp_pts[i]['x']
                indexes.append(y)
                indexes.append(x)
            edge_img = draw_edges(image_size, indexes, False)

        edge_imgs = np.append(edge_imgs, np.expand_dims(edge_img, axis=-1), axis=-1)
    print("ready")
    return edge_imgs

def draw_seg_biped(json_data, image_size, dataset_name=None):

    labels= json_data['Label']

    if 'polilinea' in labels:
        line_lab = labels['polilinea']
    else:
        line_lab = labels['Line'] if dataset_name=="gray" else labels['Polyline']
    if len(labels)>1:
        if 'Poligono' in labels:
            poligon_lab = labels['Poligono']
        else:
            poligon_lab = labels['Poligon'] if dataset_name=="gray" else labels['Polygon']
    else:
        poligon_lab = None
    # edge_img = np.zeros((image_size[0],image_size[1]))
    edge_imgs =np.zeros([image_size[0],image_size[1],1])
    indices = []
    for i in range(len(line_lab)):
        tmp_line = line_lab[i]
        if not 'polilinea' in labels:
            tmp_line = tmp_line['geometry']
        indices = []
        for x_y_points in tmp_line:
            x = x_y_points['y']  # x = tmp_pts['geometry'][i]['y'] #
            y = x_y_points['x']
            # indices.append([y,x])
            indices.append(y)
            indices.append(x)
        # print('Indeces: ', len(indices))
        if len(indices)>2:
            edge_img = draw_edges(image_size, indices, False)
            edge_imgs = np.append(edge_imgs, np.expand_dims(edge_img, axis=-1), axis=-1)
        else:
            print('No indeces: ',indices)
        # del indices, edge_img
    # polygon
    if len(labels)>1:
        for i in range(len(poligon_lab)):
            tmp_poliline = poligon_lab[i]
            if not 'Poligono' in labels:
                tmp_poliline = tmp_poliline['geometry']
            indices = []
            for x_y_points in tmp_poliline:
                x = x_y_points['y']  # x = tmp_pts['geometry'][i]['y'] #
                y = x_y_points['x']
                # indices.append([y,x])
                indices.append(y)
                indices.append(x)
            if len(indices) > 2:
                edge_img = draw_edges(image_size, indices, True)
                edge_imgs = np.append(edge_imgs, np.expand_dims(edge_img, axis=-1), axis=-1)
            else:
                print('No indeces: ', indices)
            # del indices, edge_img
    print("ready")
    del indices, edge_img
    return edge_imgs

# for drawing lines and polygons
def cv_draw_edges(img_size,xy,is_polygon=False):

    edge_img = np.zeros(img_size,dtype='uint8')
    color = (255,255,255)
    xy = np.array(xy,dtype='int32')
    # xy.reshape((-1,1,2))
    edge_img = cv.polylines(img=edge_img,pts=[xy],isClosed=is_polygon,
                            color=color,thickness=1)
    return edge_img

def draw_edges(img_size,xy,is_polygon=False):

    if is_polygon:
        edge_img = Image.new('RGB', [img_size[1], img_size[0]], "black")
        a = ImageDraw.Draw(edge_img)
        a.polygon(xy, fill='black', outline='white')
        del a
        # edge_img.save('a.png',"PNG")
        edge_img = np.array(edge_img)
        edge_img = cv.cvtColor(edge_img, cv.COLOR_RGB2GRAY)
        edge_img = np.flipud(edge_img)
        return edge_img
    else:
        edge_img = Image.new('RGB', [img_size[1], img_size[0]], "black")
        a = ImageDraw.Draw(edge_img)
        a.line(xy, fill='white')  # outline='white'
        del a
        # edge_img.save('a.png',"PNG")
        edge_img = np.array(edge_img)
        edge_img = cv.cvtColor(edge_img, cv.COLOR_RGB2GRAY)
        edge_img = np.flipud(edge_img)
        return edge_img