

import numpy as np
import os
import sys, time
import json
import platform
import shutil
import cv2 as cv

# from shapely.wkt import loads as wkt_load
from utili import data_manager as dm
from utili.data_augmentation import augment_data
from utili.BSDS_aug import augment_bsds
from utili import img2db_file as img_db
from utili.utls import *
import matplotlib.pyplot as plt

IS_LINUX = True if platform.system()=='Linux' else False

if __name__ == '__main__':

    print("Below you will find the basic operation to run: \n")
    print("Op 0: Dataset list maker .txt in json")
    print("Op 1: show the list of files in a directory")
    print("op 2: change the name of a set of files given a directory \n" )
    print("op 3: DataBase maker with the whole images data")
    print("Op 4. Given a json data, edge-map maker")
    print("Op 5. mean pixels value")
    print("Op 6. delete blank in file list train_pair_list.txt")
    print("Op 7. edge ground truth maker from n annotations")
    print("Op 8. 2012 pascal-context gt maker")
    print("Op 9. 2010 pascal-context gt maker from .mat")
    print("Op 10. 2010 pascal-context gt.mat maker from .png")
    print("Op 11. additional material dataset")
    print("Op 12. EPFL nir dataset manager 10% for testing")
    print("Op 13. data augmentation")
    print("Op 14. dataset ext changer")
    print('Op 15. Copy and paste data given a criteria')
    print('Op 16. Copy and paste data given a list of images')
    print('Op 17. Augment BSDS dataset')
    print("Op 18. Image fusion")
    print("Op 19. Gt addeding BIPED test new GT")
    print("Op 20. Addeding multiple GTs")
    print("Op 21. image inverse")


    # op = input("Please select the operation (Op): ")
    op = 13 #input("Choice one option above, then [Enter] ")
    if op==0:
        base_dir = '/opt/dataset' if IS_LINUX else '../../dataset'
        dataset_name = 'BSDS-RIND'
        img_base_dir = 'train_imgs/aug'
        gt_base_dir = 'train_gt/aug'
        save_file = os.path.join(base_dir, dataset_name)

        files_idcs = []
        for dir_name in os.listdir(os.path.join(save_file, img_base_dir)):
            img_dirs = img_base_dir + '/' + dir_name
            img_list = os.listdir(os.path.join(save_file, img_dirs))
            img_list.sort()
            gt_list = os.listdir(os.path.join(save_file, gt_base_dir,img_dirs))
            gt_list.sort()
            for k,tmp_img_name in enumerate(img_list):
                img_name = os.path.splitext(tmp_img_name)[0]
                gt_name = os.path.splitext(gt_list[k])[0]
                files_idcs.append(
                    (os.path.join(img_dirs + '/' + img_name + '.png'),
                     os.path.join(gt_base_dir + '/' + dir_name + '/' + gt_name + '.png'),))
        # save files
        save_path = os.path.join(save_file, 'train_pair.lst')
        with open(save_path, 'w') as txtfile:
            json.dump(files_idcs, txtfile)

        print("Saved in> ",save_path)

        # Check the files

        with open(save_path) as f:
            recov_data = json.load(f)
        idx = np.random.choice(2000, 1)
        print('File loaded', len(recov_data))
        tmp_files = recov_data[1500]
        img = cv.imread(os.path.join(save_file, tmp_files[0]))
        gt = cv.imread(os.path.join(save_file, tmp_files[1]))
        cv_imshow(img, 'rgb image')
        cv_imshow(gt, 'gt image')

    elif op ==1:
        dir_rgbn =  '/home/xsoria/matlabprojects/RGBrestoration/datasetRAW/un_rgbnc'  #'/opt/2kiss/raw_rgbn'
        dir_rgb =  '/home/xsoria/matlabprojects/RGBrestoration/datasetRAW/rgbnc'  #'/opt/2kiss/raw_rgb'
        dir_rgbr = '/home/xsoria/matlabprojects/RGBrestoration/datasetRAW/un_rgbr'
        # dir_nir = '/opt/2kiss/nir'
        rgbn_list = dm.list_files(dir_rgbn)# dm.list_files(dir_rgbn)
        rgb_list = dm.list_files(dir_rgb)
        rgbr_list = dm.list_files(dir_rgbr)
        # nir_list = dm.list_files(dir_nir)
        # you can enter at least one list of data and as long as 4 list of data like:
        # dm.rename_files(list, list1, list2, list3, list4, op)
        # op is the number ob operation for rename_files function
        # choice 5 to change the name in 4 directories and 6 for h5 files in three directories
        dm.rename_files(rgbn_list, rgb_list, rgbr_list,op=6)
    elif op == 3:

        dir_rgbr = '/opt/2kiss/rgbr'
        rgbr_list = dm.list_files(dir_rgbr)
        img_db.img2db(rgbr_list, dir_rgbr)

    elif op ==4:
        base_path= 'data/json'
        save_path= os.path.join(base_path,'data/biped-test')  #'/opt/2kiss/map_rgbn'  # msi or rgb
        file_name = 'biped-test.json'
        json_path = os.path.join(base_path,file_name)#'2KISS/.json'
        with open(json_path) as json_data:
            S = json.load(json_data)
            #
        j=1
        for i in range(0,len(S)): #range(201,202): # range(0,len(S))
            img_seg_name = S[i]['External ID']
            map_name = img_seg_name[:-3]+'png'
            print(img_seg_name,i)
            s = S[i]
            if s['Label']=='Skip':
                print('skip ', img_seg_name)
            else:
                edges = dm.draw_segmentation(s,[720,1280], 'SSMIHD')
                if edges is not None:

                    edges_in_one = np.sum(edges[:,:,0:edges.shape[-1]], axis=-1)
                    # cv.imshow(img_seg_name,edges_in_one)
                    # cv.waitKey(0)
                    # cv.destroyAllWindows()
                    if file_name=='ssmihd_rgbn.json':
                        edges_in_one = np.flipud(edges_in_one)
                    if img_seg_name== 'RGB_087.jpg' or img_seg_name== 'RGB_123.jpg':
                        map_name = img_seg_name[:-4] +'_'+str(int(j))+ '.png'
                        cv.imwrite(os.path.join(save_path,map_name),edges_in_one)
                        j+=1
                    else:
                        cv.imwrite(os.path.join(save_path, map_name), edges_in_one)
        # cv.imwrite('RGB_001.png',edges_in_one)
        print("finished")
    elif op ==5:
        dataset_dir ='/opt/dataset/SSMIHD/nir_train/X_un'
        nir_list = dm.list_files(dataset_dir)
        n = len(nir_list)
        acc_mean =[]
        for i in range(n):
            tmp_dir = os.path.join(dataset_dir,nir_list[i])
            tmp_img = cv.imread(tmp_dir)
            tmp_mean= np.mean(tmp_img)
            acc_mean.append(tmp_mean)

        m = len(acc_mean)
        if m==n:
            nir_mean = np.mean(acc_mean)
            print("NIR mean is: ",nir_mean)

    elif op==6:
        path ='/opt/dataset/ssmihd/edges/imgs/train/x_nir_list.txt'
        save_path = '/opt/dataset/ssmihd/edges/x_nir_list.txt'
        file_ext = '.png'
        text_file = open(path)
        file_list = text_file.readlines()
        text_file.close()
        n = len(file_list)
        new_list=[]
        if 200*288 < n:
            for i in range(n):
                if file_list[i].find(file_ext) >-1 and file_list[i].find('*') ==-1:

                    new_list.append(file_list[i])
                else:
                    print("{} no found in {}".format(file_ext,file_list[i]))

            if len(new_list)==200*288:
                with open(save_path, 'w') as f:
                    for i in (new_list):
                        f.write(i)
                print('Finished successfuly, {} files saved '.format(len(new_list)))
            else:
                print("Error")

        else:
            print("there is an inconsistency, check the file")
            sys.exit()
    elif op==7:
        base_dir='/opt/dataset'
        datasets = ['MULTICUE','PASCAL']
        dataset_name=datasets[0]
        dm.making_edge_gt(os.path.join(base_dir,dataset_name,'test','gt'))
    elif op==8:
        base_dir = '/opt/dataset/PASCAL'
        annot_dir = os.path.join(base_dir,'pascal-2012','Annotations')
        img_dir= os.path.join(base_dir,'pascal-2012','JPEGImages')
        img_save_dir = os.path.join(base_dir,'pascal-2012','imgs')
        mat_save_dir = os.path.join(base_dir,'pascal-2012','mat_edges')

        if not os.path.exists(img_save_dir):
            os.makedirs(img_save_dir)
        if not os.path.exists(mat_save_dir):
            os.makedirs(mat_save_dir)

        list_files = os.listdir(annot_dir)
        list_files.sort()
        n= len(list_files)
        raise NotImplementedError
        for i in range(n):
            name, boxes=(dm.read_xml(os.path.join(annot_dir,list_files[i])))
            print(name)
# ************ For operation 9 ****************
    elif op==9:

        base_dir = '/opt/dataset/PASCAL'
        annot_dir = os.path.join(base_dir,'train_maps')
        img_dir= os.path.join(base_dir,'pascal-2010','JPEGImages')
        img_save_dir = os.path.join(base_dir,'pascal-2010','imgs')
        mat_save_dir = os.path.join(base_dir,'pascal-2010','mat_edges')
        map_save_dir = os.path.join(base_dir, 'pascal-2010', 'edges_maps')

        if not os.path.exists(img_save_dir):
            os.makedirs(img_save_dir)
        if not os.path.exists(mat_save_dir):
            os.makedirs(mat_save_dir)
        if not os.path.exists(map_save_dir):
            os.makedirs(map_save_dir)
        first_step_finished = True

        if not first_step_finished:

            list_files = os.listdir(annot_dir)
            list_files.sort()
            n = len(list_files)
            fig = plt.figure()
            for i in range(4000,n):
                img_name, edges = dm.read_mat((os.path.join(annot_dir,list_files[i])))

                img = cv.imread(os.path.join(img_dir,img_name))
                alpha=0.0
                beta = 1.0-alpha
                a = cv.cvtColor(np.uint8(edges * 255), cv.COLOR_GRAY2BGR)
                fuse_img= cv.addWeighted(img,0.3,a,0.7,0.0)
                # cv_imshow(fuse_img)
                cv.imwrite(os.path.join(img_save_dir,img_name),img)
                cv.imwrite(os.path.join(map_save_dir,img_name[:-3]+'png'),a)
                dm.save_mat(edges,os.path.join(mat_save_dir,img_name[:-3]+'mat'), True)
                # fig.subtitle(img_name)
                if i%300==0:
                    print(img_name)
                    plt.imshow(fuse_img)
                    plt.draw()
                    plt.pause(0.2)
                else:
                    time.sleep(0.1)

                print("<<< ",i," >>>")
            plt.close()
        else:

            base_train_dir =os.path.join(base_dir,'train')
            base_test_dir = os.path.join(base_dir,'test')
            train_imgs_dir = os.path.join(base_train_dir,'imgs')
            train_mat_dir = os.path.join(base_train_dir,'gt','mat')
            train_edge_dir = os.path.join(base_train_dir,'gt','edge')
            test_imgs_dir = os.path.join(base_test_dir, 'imgs')
            test_mat_dir = os.path.join(base_test_dir, 'gt', 'mat')
            test_edge_dir = os.path.join(base_test_dir, 'gt', 'edge')

            make_dirs(train_edge_dir)
            make_dirs(train_imgs_dir)
            make_dirs(train_mat_dir)
            make_dirs(test_edge_dir)
            make_dirs(test_imgs_dir)
            make_dirs(test_mat_dir)

            imgs_list = os.listdir(img_save_dir)
            imgs_list.sort()
            mat_list = os.listdir(mat_save_dir)
            mat_list.sort()
            edge_list = os.listdir(map_save_dir)
            edge_list.sort()
            n =len(mat_list) if len(mat_list)==len(imgs_list)==len(edge_list) else 0
            all_indcs = np.arange(n)
            np.random.shuffle(all_indcs)
            test_indxs = all_indcs[:505]
            train_indcs = all_indcs[505:]
            if not len(test_indxs)+len(train_indcs)==n:
                print("Inconsistency in test and trains amples")
                sys.exit()
            # for training
            for i in train_indcs:
                if imgs_list[i][:-3]==mat_list[i][:-3]==edge_list[i][:-3]:

                    shutil.copy2(os.path.join(img_save_dir,imgs_list[i]),
                                 os.path.join(train_imgs_dir, imgs_list[i]), follow_symlinks=True)
                    shutil.copy2(os.path.join(mat_save_dir, mat_list[i]),
                                 os.path.join(train_mat_dir, mat_list[i]), follow_symlinks=True)
                    shutil.copy2(os.path.join(map_save_dir, edge_list[i]),
                                 os.path.join(train_edge_dir, edge_list[i]), follow_symlinks=True)
                else:
                    print('Error ', i)
            # for testing
            for i in test_indxs:
                if imgs_list[i][:-3] == mat_list[i][:-3] == edge_list[i][:-3]:

                    shutil.copy2(os.path.join(img_save_dir, imgs_list[i]),
                                 os.path.join(test_imgs_dir, imgs_list[i]), follow_symlinks=True)
                    shutil.copy2(os.path.join(mat_save_dir, mat_list[i]),
                                 os.path.join(test_mat_dir, mat_list[i]), follow_symlinks=True)
                    shutil.copy2(os.path.join(map_save_dir, edge_list[i]),
                                 os.path.join(test_edge_dir, edge_list[i]), follow_symlinks=True)
                else:
                    print('Error ', i)
    elif op==10:
        # pascal mat ground truth maker from png just for GT
        base_dir = '/opt/dataset/PASCALmi'
        gt_dir = os.path.join(base_dir, 'test','gt')
        png_dir = os.path.join(gt_dir, 'edge')
        mat_save_dir = os.path.join(gt_dir, 'edge_mats')

        if not os.path.exists(mat_save_dir):
            os.makedirs(mat_save_dir)
        first_step_finished = False

        if not first_step_finished:

            list_files = os.listdir(png_dir)
            list_files.sort()
            n = len(list_files)
            fig = plt.figure()
            for i in range(n):

                img = cv.imread(os.path.join(png_dir, list_files[i]))
                img_name = list_files[i]
                # cv_imshow(fuse_img)
                dm.save_mat(img, os.path.join(mat_save_dir, img_name[:-3] + 'mat'), True)
                # fig.subtitle(img_name)
                if i % 10 == 0:
                    print(img_name)
                    plt.imshow(img)
                    plt.draw()
                    plt.pause(0.2)
                else:
                    time.sleep(0.1)

                print("<<< ", i, " >>>")
            plt.close()
    elif op==12:
        np.random.seed(7)
        base_dir = '/opt/dataset/EPFL'
        test_dir = os.path.join(base_dir, 'test')
        train_dir = os.path.join(base_dir, 'train')
        all_dir_list = os.listdir(os.path.join(base_dir,'all'))
        n =0
        for i in range(len(all_dir_list)):
            tmp_n = os.listdir(os.path.join(base_dir, 'all', all_dir_list[i]))
            n+=len(tmp_n)//2
        n_test = n*0.1
        n_test = n_test//len(all_dir_list)
        n_test = int(n_test) if n_test%2==0 else int(n_test+1)
        k = 0
        for i in range(len(all_dir_list)):
            tmp_img_list = os.listdir(os.path.join(base_dir, 'all', all_dir_list[i]))
            tmp_img_list.sort()
            test_idcs = np.random.choice(len(tmp_img_list), n_test)
            for j in range(0,len(tmp_img_list),2):

                if j in test_idcs:
                    y_dir =os.path.join(test_dir,'nir') # to save nir images
                    x_dir = os.path.join(test_dir,'rgb') # to save rgb images
                    rgb_name = tmp_img_list[j+1]
                    nir_name = tmp_img_list[j]
                    print('No. ',k+1,' data saving for testing:')
                else:
                    y_dir = os.path.join(train_dir, 'nir')  # to save nir images
                    x_dir = os.path.join(train_dir, 'rgb')  # to save rgb images
                    rgb_name = tmp_img_list[j + 1]
                    nir_name = tmp_img_list[j]
                    print('No. ', k + 1,'data saving for training:')

                if rgb_name[:-8] == nir_name[:-8]:
                    pass
                else:
                    rgb_name = tmp_img_list[j - 1]
                    if rgb_name[:-8] == nir_name[:-8]:
                        pass
                    else:
                        raise SyntaxError
                x_name = 'rgb_' + format(k + 1, '03')
                y_name = 'nir_' + format(k + 1, '03')
                nir = cv.imread(os.path.join(base_dir, 'all', all_dir_list[i], nir_name))
                rgb = cv.imread(os.path.join(base_dir, 'all', all_dir_list[i], rgb_name))
                cv.imwrite(os.path.join(y_dir, y_name + '.png'),nir)
                cv.imwrite(os.path.join(x_dir, x_name + '.jpg'),rgb)
                print(x_dir)
                k+=1

        print(n," pair of images successfully generated")
    elif op==13:
        base_dir= '/opt/dataset' if IS_LINUX else '../../dataset'
        augment_both=True# to augment the input and target
        augment_data(base_dir=base_dir,augment_both=augment_both, use_all_augs=True)

    elif op == 14:
        base_dir ='/opt/dataset/CID/imgs'
        list_imgs = os.listdir(base_dir)
        n = len(list_imgs)
        for i in range(n):

            tmp_img = cv.imread(os.path.join(base_dir,list_imgs[i]))
            tmp_name,_ = os.path.splitext(list_imgs[i])
            tmp_name = tmp_name+'.png'

            cv.imwrite(os.path.join(base_dir,tmp_name), tmp_img)

    elif op == 15:

        selec_num = [54,76,95,96,1,2,3,7,9,14,16,17,20,27,30,42,64,78,79,87] # for test MULTICUE sellection
        dest_dir = '/opt/dataset/SSMIHD/edges/edge_maps/test/rgbr'
        source_dir = '/opt/dataset/SSMIHD/edges/edge_maps/rgbv6'
        ref_dir = '/opt/dataset/SSMIHD/edges/edge_maps/test/rgbrv4'
        source_list = os.listdir(source_dir)
        source_list.sort()
        ref_list = os.listdir(ref_dir)
        ref_list.sort()

        for i in range(len(ref_list)): #selec_num: #
            tmp_name = ref_list[i]
            tmp_name,_ = os.path.splitext(tmp_name)
            # tmp_name = tmp_name+'.png'
            shutil.copy2(os.path.join(source_dir, tmp_name+'.png'),
                         os.path.join(dest_dir, tmp_name+'.png'), follow_symlinks=True)
        print('files copy finished successfully')

    elif op == 16:
        # copy data from a folder to different dirs test and train
        base_dir= '/opt/dataset/BIPED/edges/edge_maps'
        test_name_list = os.listdir(os.path.join(base_dir,'test','rgbrv4'))
        train_name_list = os.listdir(os.path.join(base_dir,'train','rgbr', 'realv4'))
        gt_dir = os.path.join(base_dir,'rgbv6')
        test_save_dir = os.path.join(base_dir,'test','rgbr')
        train_save_dir = os.path.join(base_dir,'train','rgbr', 'real')
        os.makedirs(test_save_dir, exist_ok=True)
        os.makedirs(train_save_dir, exist_ok=True)
        # train copy
        for i in range(len(train_name_list)):
            tmp_name = train_name_list[i]
            tmp_name, _ = os.path.splitext(tmp_name)
            # tmp_name = tmp_name+'.png'
            shutil.copy2(os.path.join(gt_dir, tmp_name + '.png'),
                         os.path.join(train_save_dir, tmp_name + '.png'), follow_symlinks=True)
            print(os.path.join(gt_dir, tmp_name + '.png'), "to ", os.path.join(train_save_dir, tmp_name + '.png'))
        # test copy
        for i in range(len(test_name_list)):
            tmp_name = test_name_list[i]
            tmp_name, _ = os.path.splitext(tmp_name)
            # tmp_name = tmp_name+'.png'
            shutil.copy2(os.path.join(gt_dir, tmp_name + '.png'),
                         os.path.join(test_save_dir, tmp_name + '.png'), follow_symlinks=True)
            print(os.path.join(gt_dir, tmp_name + '.png'), "to ", os.path.join(test_save_dir, tmp_name + '.png'))


    elif op == 17:
        # base_dir = '/opt/dataset' if IS_LINUX else '../../dataset'
        base_dir = '/opt/dataset' if IS_LINUX else "C:/Users/xavysp/dataset"
        augment_both = True
        dataset = "BSDS-RIND"# to augment the input and target
        augment_bsds(base_dir=os.path.join(base_dir,dataset),
                     augment_both=augment_both, use_all_augs=True)

    elif op==18:
        base_dir = '/opt/dataset' if IS_LINUX else "C:/Users/xavysp/dataset/BIPED/edges"
        img_fusing(base_dir=base_dir)
    elif op==19:
        base_dir = '/opt/dataset' if IS_LINUX else "C:/Users/xavysp/dataset/BIPED/edges"
        path1= os.path.join(base_dir,'edge_maps','train','rgbr', 'real')
        path2 = 'data/GTs'
        adding_2imgs(path1,path2,base_dir)
    elif op==20:
        base_dir = '/opt/dataset' if IS_LINUX else "C:/Users/xavysp/dataset"
        dataset_dir = os.path.join(base_dir,'BSDS-RIND')
        gt_dirs= os.path.join(dataset_dir, 'train')
        list_gt_dirs = os.listdir(gt_dirs)
        adding_Nimgs(dataset_dir, gt_dirs, list_gt_dirs)

    elif op==21:
        base_dir = '/opt/dataset' if IS_LINUX else "C:/Users/xavysp/dataset"
        dataset_dir = os.path.join(base_dir,'BIPED')
        gt_dir = os.path.join(dataset_dir,'edges',
                              'edge_maps','test','rgbr') #
        # gt_dir = os.path.join(base_dir, 'BIPED', 'edges')
        list_gt = os.listdir(gt_dir)

        save_dir = os.path.join(dataset_dir,'vis_test_gt')
        os.makedirs(save_dir,exist_ok=True)
        for path in list_gt:
            gt = cv.imread(os.path.join(gt_dir,path))
            inv_gt = 255-gt
            cv.imwrite(os.path.join(save_dir,path), inv_gt)

    else:
        img_base_dir = '/opt/dataset/MULTICUE/test/imgs'
        gt_dir = '/opt/dataset/MULTICUE/train/gt'
        img_dir = '/opt/dataset/MULTICUE/train/imgs'
        mat_dir = '/opt/dataset/MULTICUE/train/mats'
        listdel = os.listdir(img_base_dir)
        for i in listdel:
            file_name, _ = os.path.splitext(i)
            os.remove(os.path.join(gt_dir,file_name+'.png'))
            os.remove(os.path.join(img_dir,file_name+'.png'))
            os.remove(os.path.join(mat_dir,file_name+'.mat'))

        print("Ready")
