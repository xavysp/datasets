import numpy as np
import cv2 as cv
import os

def cv_imshow(img,title='image'):
    print(img.shape)
    cv.imshow(title,img)
    cv.waitKey(0)
    cv.destroyAllWindows()

def gamma_correction(i, g,gamma=True):
    """
    0.4040 0.3030 0.6060
    :param i: image data
    :param g: gamma value
    :param gamma: if true do gamma correction if does not degamma correction
    :return:gamma corrected image if false image without gamma correction
    """
    i = np.float32(i)
    if gamma:
        img=i**g
    else:
        img=i**(1/g)
    return img

def image_normalization(img, img_min=0, img_max=255):
    """This is a typical image normalization function
    where the minimum and maximum of the image is needed
    source: https://en.wikipedia.org/wiki/Normalization_(image_processing)
    :param img: an image could be gray scale or color
    :param img_min:  for default is 0
    :param img_max: for default is 255
    :return: a normalized image, if max is 255 the dtype is uint8
    """
    epsilon=1e-12 # whenever an inconsistent image
    img= img.astype('float32')
    img = (img-np.min(img))*(img_max-img_min)/((np.max(img)-np.min(img))+epsilon)+img_min
    return np.float32(img)

def make_dirs(paths): # make path or paths dirs
    if not os.path.exists(paths):
        os.makedirs(paths)
        print("Directories have been created: ",paths)
        return True
    else:
        print("Directories already exists: ", paths)
        return False

def edge_maker(im):
    h,w=im.shape
    map1 = np.zeros((h,w))
    map2 = np.zeros((h, w))
    for i in range(h-1):
        for j in range(w-1):
            map1[i,j]=0 if im[i,j]==im[i,j+1] else 1
    for j in range(w-1):
        for i in range(h-1):
            map2[i,j]=0 if im[i,j]==im[i+1,j] else 1

    img=map1+map2
    return img

def make_dirs(dirs_path):
    if not os.path.exists(dirs_path):
        os.makedirs(dirs_path)
        print(dirs_path," dirs created!")
    else:
        print(dirs_path," already exist!")

