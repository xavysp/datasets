""" New boundary dataset generation

The codes below contain the process and instruction to generate or cut the
new image for boundary detection
"""
import numpy as np
import cv2 as cv
import os
import shutil
import json, h5py
from data_utils import draw_seg_wibd, draw_seg_biped
import platform
from scipy.io import loadmat
from utili.utls import image_normalization

is_linux=True if platform.system()=="Linux" else False

def cv_imshow(label="image",img=None, read_image=False):

	if not read_image:
		cv.imshow(label,img)
		cv.waitKey(0)
		cv.destroyAllWindows()
	else:
		cv.imshow(label, cv.imread(img))
		cv.waitKey(0)
		cv.destroyAllWindows()
# def make_dir(dirs):
# 	if not os.path.isdir(dirs):
# 		os.ma
def image_cut(img_path):
	img = cv.imread(img_path)
	img_shape = img.shape
	print(img_shape)
	if img_shape[0]<img_shape[1]:
		img_cut = cv.resize(img, (img_shape[1] - (img_shape[0] - 800), 800))
	else:
		print('error',img_path)
		img_cut = cv.resize(img, (800, img_shape[0] - (img_shape[1] - 800)))
	return img_cut

def image_processing(img_path):

	control=0
	while control==0:

		img = cv.imread(img_path)
		print(img.shape)
		img_shape_name = str(img.shape[0])+'_'+str(img.shape[1])
		cv.imshow(img_shape_name, img)
		cv.waitKey(0)
		cv.destroyAllWindows()
		s_point = int(input('Input x axis to cut image: '))
		img_shape = img.shape
		# now cut the image in 1280x720
		if s_point> 0:
			if img_shape[1] > 1280:
				cut_img = img[:,1:1+1280,:]
			elif img_shape[1]>1249 and img_shape[1]<1280:
				cut_img = img[:,1:1+1248,:]
			else:
				cut_img = img[:, 0:0+1024, :]
		else:
			img_shape = img.shape
			if img_shape[1]>1280:
				s_point = (img_shape[1]//2-(1280//2))
				cut_img = img[:,s_point:s_point+1280,:]
			elif img_shape[1]>1249 and img_shape[1]<1280:
				s_point = (img_shape[1] // 2 - (1248 // 2))
				cut_img = img[:, s_point:s_point + 1248, :]
			else:
				s_point = (img_shape[1] // 2 - (1024 // 2))
				cut_img = img[:, s_point:s_point + 1024, :]

		cv.imshow(img_path, cut_img)
		cv.waitKey(0)
		cv.destroyAllWindows()
		accept_img =int(input("Is that image correct: ( put 1 if so) "))
		if accept_img==1:
			control=1
		else:
			control=0

	return cut_img

def data_generation(base_dir, dataset_name):

	data_dir = os.path.join(base_dir,dataset_name)
	list_imgs = os.listdir(data_dir)
	n = len(list_imgs)
	new_name = 'WIBD'
	new_data_dir = os.path.join(base_dir,new_name)

	if not os.path.isdir(new_data_dir):
		os.makedirs(new_data_dir)
	tmp_min=2000
	for i in range(223,n):
		img_dir = os.path.join(data_dir,list_imgs[i])
		_, tmp_fileExt = os.path.splitext(img_dir)
		img = cv.imread(img_dir)
		img_shape = img.shape
		if img_shape[0]<800 or img_shape[1]<800:
			img_name = "WIBD_"+str(img_shape[0])+'_'+str(img_shape[1])+'_'+'{:03}'.format(i+1)+'.png'
		else:
			img_name ="WIBD_"+'{:03}'.format(i+1)+'.png'
		tmp_img=image_processing(img_dir)
		# tmp_img = image_cut(img_dir)
		# if tmp_img.shape[1]<tmp_min:
		# 	tmp_min =tmp_img.shape[1]
		# save new image
		cv.imwrite(os.path.join(new_data_dir,img_name),tmp_img)
		print(tmp_img.shape, 'number', i, img_name)
		# print(img.shape,'idx:',i,list_imgs[i])
		# print("data processed: ", new_name+'_' +num2str(i)+'.png')

	print("The minimum width image: ",tmp_min)

def split_data(base_dir, dataset_name,test_ids=None):

	dataset_dir = os.path.join(base_dir, dataset_name)
	all_data_dir = os.path.join(dataset_dir,"all_data")
	train_dir = os.path.join(dataset_dir,"train")
	test_dir = os.path.join(dataset_dir,"test")
	new_test_dir = os.path.join(dataset_dir,"new_test")
	list_dir = os.listdir(all_data_dir)

	os.makedirs(train_dir,exist_ok=True)
	os.makedirs(test_dir,exist_ok=True)
	os.makedirs(new_test_dir,exist_ok=True)

	# copy data for train dir
	for indx in range(len(list_dir)):
		file_name = list_dir[indx]
		shutil.copy2(os.path.join(all_data_dir,file_name),
		train_dir)
		print(os.path.join(train_dir,file_name))
	# delete test data in train dir
	for indx in test_ids:
		file_name = list_dir[indx-1]
		shutil.move(os.path.join(train_dir,file_name),
					os.path.join(test_dir,file_name))
		print(os.path.join(test_dir,file_name))
	# resize test images
	test_list = os.listdir(test_dir)
	sum = 0
	for i in range(len(test_list)):
		tmp_img = cv.imread(os.path.join(test_dir,test_list[i]))
		tmp = tmp_img
		print(test_list[i], "image size is: ",tmp_img.shape)
		if tmp_img.shape[1]<1050:
			sum+=1
			# cv.resize(tmp_img, (2014, 800))
		# cv.imwrite(os.path.join(new_test_dir,test_list[i]),tmp)
	print("total of 1280: ",sum)

def resize_dataset(img_list=None, dataset_name=None, base_dir=None):

	# print("800x1280")
	base_dir = os.path.join(base_dir,dataset_name)
	raw_dir = os.path.join(base_dir,'raw_data')
	save_dir = os.path.join(base_dir,'resized_data')
	os.makedirs(save_dir,exist_ok=True)
	cut_img =True
	resize_image =False
	k=1
	for file_name in img_list:

		tmp_img = cv.imread(os.path.join(raw_dir,file_name+'.png'))
		if resize_image:
			(wt, ht) = [1460, 980]  # target image size
			(h, w) = tmp_img.shape[:2]  # given image size
			fx = w / wt
			fy = h / ht
			f = max(fx, fy)
			newSize = (max(min(wt, int(w / f)), 1),
					   max(min(ht, int(h / f)), 1))
			resize_img = cv.resize(tmp_img,newSize,interpolation=cv.INTER_CUBIC)
			print("image data: ", file_name,"index ", k)
			print("current image size: ", resize_img.shape)
			k += 1
		if cut_img:
			(wt, ht) = [1280, 800]
			count=0
			while count==0:
				print(tmp_img.shape)
				cv_imshow('normal image', tmp_img)
				x_axis = int(input("Input X axis:"))
				y_axis = int(input("Input Y axis:"))

				resize_img = tmp_img[y_axis:y_axis+800,x_axis:x_axis+1280]
				print("cutted datat",resize_img.shape)
				cv_imshow('cutted image',resize_img)
				res = int(input("Are you ok with that image, if so press 1 otherwise 0:"))
				if res==1:
					count=1

		cv.imwrite(os.path.join(save_dir,file_name+'.png'), resize_img)
		print('saved data in: ',os.path.join(save_dir,file_name+'.png'))

def png2jpg(base_dir, dataset, imgs_dir):

	base_dir = os.path.join(base_dir,dataset)
	list_imgs = os.listdir(os.path.join(base_dir,imgs_dir))
	save_dir = os.path.join(base_dir,'all_data_jpg')
	os.makedirs(save_dir,exist_ok=True)
	for img_name in list_imgs:
		tmp= cv.imread(os.path.join(base_dir,imgs_dir,img_name))
		cv.imwrite(os.path.join(save_dir,img_name[:-3]+'jpg'), tmp)
		print('image saved: ',os.path.join(save_dir,img_name[:-3]+'jpg'))
	print("Conversion finished")

def genGT4wibd(base_dir,file_dir,file_name):
	json_path = os.path.join(base_dir,file_dir,file_name)
	img_dir = os.path.join(base_dir,'all_data_jpg')
	save_path = os.path.join(base_dir,file_dir,'GTs')
	os.makedirs(save_path,exist_ok=True)
	with open(json_path) as json_data:
		S = json.load(json_data)
	#
	j = 1
	for i in range(0, len(S)):  # range(201,202): # range(0,len(S))
		img_seg_name = S[i]['External ID']
		map_name = img_seg_name[:-3] + 'png'
		print(img_seg_name, i)
		s = S[i]
		if s['Label'] == 'Skip':
			print('skip ', img_seg_name)
		else:
			edges = draw_seg_wibd(s, [800, 1280], 'omnib')
			if edges is not None:

				edges_in_one = np.sum(edges[:, :, 0:edges.shape[-1]], axis=-1)
				edges_in_one = np.flipud(edges_in_one)
				cv.imshow(img_seg_name,edges_in_one)
				cv.waitKey(0)
				cv.destroyAllWindows()
				# blend data
				# img = cv.imread(os.path.join(img_dir,img_seg_name),cv.IMREAD_GRAYSCALE)
				# bled_img = cv.addWeighted(np.uint8(edges_in_one), 0.3, img, 0.7, 0)
				# cv.imshow(img_seg_name, bled_img)
				# cv.waitKey(0)
				# cv.destroyAllWindows()
				if file_name == 'ssmihd_rgbn.json':
					edges_in_one = np.flipud(edges_in_one)
				if img_seg_name == 'RGB_087.jpg' or img_seg_name == 'RGB_123.jpg':
					map_name = img_seg_name[:-4] + '_' + str(int(j)) + '.png'
					cv.imwrite(os.path.join(save_path, map_name), edges_in_one)
					j += 1
				else:
					cv.imwrite(os.path.join(save_path, map_name[:-3]+'png'), edges_in_one)
	# cv.imwrite('RGB_001.png',edges_in_one)
	print("finished")

def genGT4biped(base_dir,file_dir,file_name):
	json_path = os.path.join(base_dir,file_dir,file_name)
	if is_linux:
		img_base_dir = os.path.join('..','..','dataset','BIPED','edges','imgs')
	else:
		img_base_dir=os.path.join('/opt','dataset','BIPED','edges','imgs')
	img_dir = os.path.join(img_base_dir,'test','rgbn')
	save_path = os.path.join(base_dir,'GTs')
	os.makedirs(save_path,exist_ok=True)
	with open(json_path) as json_data:
		S = json.load(json_data)
	#
	dataset_name = 'gray' if len(S)<300 else 'other' # for BIPED <300 other wise 100
	j = 1
	for i in range(107, len(S)):  # range(201,202): # range(0,len(S))
		img_seg_name = S[i]['External ID']
		map_name = img_seg_name[:-3] + 'png'
		print(img_seg_name, i)
		s = S[i]
		if s['Label'] == 'Skip':
			print('skip ', img_seg_name)
		else:
			edges = draw_seg_biped(s, [720, 1280], dataset_name)
			if edges is not None:

				edges_in_one = np.sum(edges[:, :, 0:edges.shape[-1]], axis=-1)
				if dataset_name!="gray":
					edges_in_one = np.flipud(edges_in_one)
				# cv.imshow(img_seg_name,edges_in_one)
				# cv.waitKey(0)
				# cv.destroyAllWindows()
				# blend data
				# img = cv.imread(os.path.join(img_dir,img_seg_name[:-3]+'jpg'),cv.IMREAD_GRAYSCALE)
				# bled_img = cv.addWeighted(np.uint8(edges_in_one), 0.3, img, 0.7, 0)
				# cv.imshow(img_seg_name, bled_img)
				# cv.waitKey(0)
				# cv.destroyAllWindows()
				if img_seg_name == 'RGB_087.jpg' or img_seg_name == 'RGB_123.jpg':
					map_name = img_seg_name[:-4] + '_' + str(int(j)) + '.png'
					cv.imwrite(os.path.join(save_path, map_name), edges_in_one)
					j += 1
				else:
					cv.imwrite(os.path.join(save_path, map_name[:-3]+'png'), edges_in_one)
		# del edges_in_one, edges

	print("finished")


if __name__=='__main__':

	base_dir = '/opt/dataset' if platform.system()=='Linux' else '../../dataset'

	DATASET_NAME = "WIBD"
	data_dir = 'all_data'
	# operations equals to op = 1, data splitting, op=2, data generation,
	# op = 0 pascal ata maker
	# op=3 resize images, op=4, png to jpg, op=5, GT generation boundaries
	# op = 6 Gt generation for BIPED or MBIPED
	# op = 7 blend images
	op = 0
	if op==0:
		data_dir = os.path.join(base_dir, 'PASCAL', 'trainval','trainval')
		list_dir = os.listdir(data_dir)
		for i in list_dir:
			f= loadmat(os.path.join(data_dir,i))
			mat_data = f['LabelMap']
			img = np.uint8(image_normalization(mat_data))
			cv_imshow(img=img)


	elif op==1:
		# split dataser
		# test_ids= [1,6,9,12,14,29,34,57,59,64,72,88,94,100,112,113,
		# 		   115,132,135,140,148,149,155,167,168,177,179,181,
		# 		   183,186,189,194,196,200,209,210,212,220,222,230,
		# 		   232,243,245,248,249,250,251,255,256,262,263,265,
		# 		   268,271,272,273,274,277,279,286,288,302,307,308,
		# 		   320,321,329,342,346,347] # 70 test images
		test_ids = [1, 6, 9, 12, 14, 29, 34, 57, 59, 64, 72, 88, 94, 100, 112, 113,
					148, 149,230,232, 243, 245, 248, 249, 250, 251, 255, 256, 262,
					263, 265, 268, 271, 272, 273, 274, 277, 279, 286, 288, 302, 307, 308,
					320, 321,327, 329, 342, 346, 347]
		split_data(base_dir=base_dir,dataset_name=DATASET_NAME,test_ids=test_ids)

	elif op==2:

		# dataset_name ='new_wibd'
		data_generation(base_dir, DATASET_NAME)
		print("Data generated successfuly")

	elif op==3:
		# resize image
		# img_list=['WIBD_014','WIBD_025','WIBD_033','WIBD_044','WIBD_047',
		# 		  'WIBD_049','WIBD_050','WIBD_054','WIBD_057','WIBD_059',
		# 		  'WIBD_064','WIBD_066','WIBD_069','WIBD_077','WIBD_084',
		# 		  'WIBD_087','WIBD_092','WIBD_118','WIBD_138','WIBD_170',
		# 		  'WIBD_211','WIBD_214','WIBD_223','WIBD_227','WIBD_344']
		img_list = ['WIBD_059','WIBD_069', 'WIBD_077',	'WIBD_087']
		# img_list = ['WIBD_054']

		resize_dataset(img_list, DATASET_NAME, base_dir)
		print("Data resized successfuly")
	elif op==4:
		png2jpg(base_dir,DATASET_NAME,data_dir)
	elif op==5:

		file_dir = 'data/jsons'
		file_name='wibdv1.json'
		base_dir = os.path.join(base_dir,DATASET_NAME)
		genGT4wibd(base_dir,file_dir,file_name)
	elif op==6:
		base_dir = 'data'
		file_dir = 'jsons'
		file_name = 'BIPEDv6.json' #'Fus_segLASTmbiped1.json' #'Gray_segLASTmbiped1.json' # 'Gray_segLASTmbiped1'
		genGT4biped(base_dir, file_dir, file_name)
	elif op==7:
		img_path=os.path.join('/opt/dataset','BIPED','edges/imgs/rgbr')
		gt_path = os.path.join('data','GTs')
		list_img = os.listdir(img_path)
		list_gt = os.listdir(gt_path)
		list_img.sort()
		list_gt.sort()
		for i in range(12,len(list_gt)):
			tmp_img = cv.imread(os.path.join(img_path,list_img[i]))
			tmp_gt = cv.imread(os.path.join(gt_path,list_gt[i]))
			bled_img = cv.addWeighted(tmp_gt, 0.5, tmp_img, 0.7, 0)
			cv_imshow(label=list_img[i],img=bled_img)
			print('Done ', list_img[i])


